function main() {
  const data = {
    type: "sc-card",
    label: "Factures client",
    dt: "Sales Invoice",
    button: {
      view: "List",
    },
    items: [
      {
        type: "sc-foryou",
        items: [
          {
            type: "sc-link-pill",
            className: "sc-foryou-like",
            filters: [["_liked_by", "like", `%"thierry@dokos.io"%`]],
            counter: {
              // format: "{0} favoris",
              format: "{0}",
            },
          },
          {
            type: "sc-link-pill",
            className: "sc-foryou-assign",
            filters: [["_assign", "like", `%"thierry@dokos.io"%`]],
            counter: {
              format: "{0} assignés",
            },
          },
        ],
      },
      {
        type: "sc-section",
        label: "Ce mois-ci",
        filters: [["modified", ">", 42]],
        items: [
          {
            type: "sc-link",
            filters: [["docstatus", "=", 2]],
            counter: true,
            label: "Impayées",
          },
          {
            type: "sc-link",
            filters: [["docstatus", "=", 0]],
            counter: true,
          },
          {
            type: "sc-link",
            filters: [["docstatus", "=", 1]],
            counter: true,
            label: "Validées",
          },
        ],
      },
      {
        type: "sc-section",
        label: "Documents liés",
        items: [
          {
            type: "sc-link",
            className: "sc-extlink",
            dt: "Sales Order",
            filters: [["invoicing_status", "=", 42]],
            label: "Commandes client",
            counter: {
              // format: "{0} To Invoice",
              label: "À facturer",
            },
          },
        ],
      },
    ],
  };

  /**
   * @typedef {object} SCItem
   * @property {"sc-card", "sc-section", "sc-link", "sc-foryou"} type
   * @property {SCItem[]} items
   */

  function fillSlot(slotParent, slotName, slotContent) {
    const slot = slotParent.querySelector(`slot[name="${slotName}"]`);
    if (!slot) {
      return;
    }
    slot.append(slotContent);
  }

  function inferDocType(data) {
    let dt = data.dt;
    if (dt) {
      return dt;
    }

    let p = data._parent;
    while (p) {
      dt = p.dt;
      p = p._parent;

      if (dt) {
        return dt;
      }
    }

    throw new Error("Summary Card must have a doctype");
  }

  function inferLabel(data) {
    let label = data.label;
    if (label) {
      return label;
    }

    const inferred = data._inferred;
    const parent = data._parent;
    if (parent && parent._inferred.dt === inferred.dt) {
      label = inferLabelFromFilters(data);
      if (label) {
        return label;
      }
    }

    return label;
  }

  const operators = {
    "=": (a, b) => a == b,
    like: (a, b) => a.toLowerCase().includes(b.toLowerCase()),
    "not like": (a, b) => !a.toLowerCase().includes(b.toLowerCase()),
  };
  for (let op of ["!=", ">", "<", ">=", "<="]) {
    operators[op] = new Function("a", "b", `return a ${op} b`);
  }

  function applyFilteringOperatorToMock(opName, operand, universe) {
    const operator = operators[opName];
    const filtered = universe.filter((item) => operator(item, operand));
    return filtered;
  }

  function inferLabelFromFilters(data) {
    if (!data.filters || !data.filters.length) {
      return;
    }

    const firstFilter = data.filters[0];
    if (!firstFilter || firstFilter.length !== 3) {
      return;
    }

    const [field, operator, value] = firstFilter;

    if (field === "docstatus") {
      const res = applyFilteringOperatorToMock(operator, value, [0, 1, 2]);
      if (res.length) {
        // const docStatuses = ["Draft", "Submitted", "Cancelled"];
        const docStatuses = ["Brouillon", "Validé", "Annulé"];
        return res.map((v) => docStatuses[v]).join(", ");
      }
    }

    const label = `${field} ${operator} ${value}`;
    return label;
  }

  function inferCounter(data) {
    if (!data.counter) {
      return;
    }

    let fmt = "{0}";
    if (typeof data.counter === "object") {
      if (data.counter.format && data.counter.label) {
        throw new Error("Counter cannot have both `format` and `label`");
      }
      if (data.counter.format) {
        fmt = data.counter.format;
      }
      if (data.counter.label) {
        fmt = "{0} " + data.counter.label;
      }
    } else if (Boolean(data.counter) === true) {
      fmt = "{0}";
    }

    const value = computeCounterValue(data);
    const label = fmt.replace("{0}", value);
    return label;
  }

  function inferMainButtonLabel(card) {
    if (!card.button) {
      return;
    }
    if (typeof card.button !== "object") {
      throw new Error("Button must be an object");
    }
    if (card.button?.label) {
      return card.button.label;
    }
    if (card.button?.view) {
      return card.button.view;
    }
    return "Button";
  }

  const inferers = {
    dt: inferDocType,
    label: inferLabel,
    counter: inferCounter,
    btn_label: inferMainButtonLabel,
  };

  function computeCounterValue(data) {
    return Math.round(Math.pow(Math.random(), 3) * 50 + Math.random() * 10);
  }

  /**
   * @param {SCItem} node
   * @param {Optional<SCItem>} parent
   * @param {HTMLElement} container
   */
  function iterate(data, parent, htmlContainer) {
    const { type, items } = data;
    data._parent = parent;

    // Fetch data
    const inferred = {};
    data._inferred = inferred;
    // Fill out missing data
    const keys = ["dt", "label", "counter"];

    if (type === "sc-card") {
      keys.push("btn_label");
    }

    for (const key of keys) {
      if (inferers[key]) {
        inferred[key] = inferers[key](data);
      }
    }

    // Rendering

    /** @type {HTMLTemplateElement} */
    const template = document.querySelector(`template[name="${type}"]`);
    const fragment = template.content.cloneNode(true);
    const wrapper = document.createElement("div");
    wrapper.appendChild(fragment);

    const mainElement = wrapper.firstElementChild;
    const itemsSlot = wrapper.querySelector(`slot[name="items"]`);

    if (data.className) {
      mainElement.classList.add(data.className);
    }

    if (items?.length) {
      if (!itemsSlot) {
        console.error(data);
        throw new Error("Items slot not found in template: " + type);
      }
      items.forEach((child) => {
        iterate(child, data, itemsSlot);
      });
    }

    for (const [slotName, slotContent] of Object.entries(inferred)) {
      if (slotContent) {
        fillSlot(wrapper, slotName, slotContent);
      }
    }

    // Delete remaining slots
    const slots = wrapper.querySelectorAll("slot");
    for (const slot of slots) {
      slot.outerHTML = slot.innerHTML;
    }

    // Move the child nodes of `div` to the container
    while (wrapper.firstChild) {
      htmlContainer.appendChild(wrapper.firstChild);
    }
  }

  iterate(data, null, document.getElementById("test1"));
}
main();
